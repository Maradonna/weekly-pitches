from pybaseball import playerid_lookup
from bs4 import BeautifulSoup
import requests
import shutil
import math


def savant_clip(pitch, date_min, date_max):
    clip_url = "https://baseballsavant.mlb.com/statcast_search?hfPT=&hfAB=&hfBBT=&hfPR=&hfZ=&stadium=&hfBBL=&hfNewZones=&hfGT=R%7CPO%7CS%7C&hfC=[count]%7C&hfSea=[season]%7C&hfSit=&player_type=pitcher&hfOuts=[outs]%7C&opponent=&pitcher_throws=&batter_stands=&hfSA=&game_date_gt=[date_min]&game_date_lt=[date_max]&hfInfield=&team=&position=&hfOutfield=&hfRO=&home_road=&hfFlag=&hfPull=&pitchers_lookup%5B%5D=[pitcher_id]&metric_1=api_p_release_speed&metric_1_gt=[min_speed]&metric_1_lt=[max_speed]&metric_2=api_p_release_spin_rate&metric_2_gt=[spinrate]&metric_2_lt=[spinrate]&hfInn=[Inning]|&min_pitches=0&min_results=0&group_by=name&sort_col=pitches&player_event_sort=h_launch_speed&sort_order=desc&min_pas=0&type=details&player_id=[pitcher_id]"
    p_name = pitch['player_name'].replace(',', '').split()
    if len(p_name) > 2:
        f_name = ' '.join(p_name[:-1])
        l_name = p_name[-1]
        p_name = [f_name, l_name]
    p_id = playerid_lookup(p_name[0], p_name[1])['key_mlbam'].values
    pitch_map = {
        "[Inning]": int(pitch['inning']),
        "[pitcher_id]": p_id[0],
        "[date_min]": date_min,
        "[date_max]": date_max,
        "[count]": str(int(pitch['balls']))+str(int(pitch['strikes'])),
        "[season]": "2021",
        "[outs]": int(pitch['outs_when_up']),
        "[min_speed]": int(pitch["release_speed"]-1),
        "[max_speed]": int(pitch["release_speed"]+1),
        "[spinrate]": int(pitch["release_spin_rate"])
    }
    for k, v in pitch_map.items():
        clip_url = clip_url.replace(k, str(v))
    print(clip_url)
    site = requests.get(clip_url)
    soup = BeautifulSoup(site.text, features="lxml")
    for link in soup.find_all('a'):
        clip_savant = requests.get("https://baseballsavant.mlb.com"+link.get('href'))
        clip_soup = BeautifulSoup(clip_savant.text, features='lxml')
        video_obj = clip_soup.find("video", id="sporty")
        clip_url = video_obj.find('source').get('src')
        return clip_url


def download_file(url):
    local_filename = url.split("/")[-1]
    with requests.get(url, stream=True) as r:
        with open(local_filename, "wb") as f:
            shutil.copyfileobj(r.raw, f)
    return local_filename


def calc_zone_distance(inp):
    x, y = inp[0], inp[1]
    x_min = -0.708
    x_max = 0.708
    y_max = inp[2]
    y_min = inp[3]
    if x <= x_min:
        if y <= y_min:
            return math.sqrt((x_min-x)**2 + (y_min-y)**2)
        elif(y > y_max):
            return math.sqrt((x_min-x)**2 + (y_max-y)**2)
        elif((y >= y_min) & (y <= y_max)):
            return x_min - x
    else:
        if ((x > x_min) & (x < x_max)):
            if (y < y_min):
                return y_min - y
            elif (y > y_max):
                return y - y_max
        else:
            if (x > x_max):
                if (y < y_min):
                    return math.sqrt((x_max-x)**2 + (y_min-y)**2)
                elif(y > y_max):
                    return math.sqrt((x_max-x)**2 + (y_max-y)**2)
                elif((y >= y_min) & (y <= y_max)):
                    return x - x_max
    return 0.0
